<?php
session_start(); // Demarrage de la session

// Vérification de l'existance d'une session
// Permet d'eviter d'aller sur une page avec son url sans sessions
if(!isset($_SESSION['profil'])){
    header('Location: index.php');
}

// Vérification de la duree de la session
if (!isset($_SESSION['timeout_idle'])) {
    $_SESSION['timeout_idle'] = time() + 2*24*60*60; // Si existe pas on créer pour 48h
}
else {
    if ($_SESSION['timeout_idle'] < time()) {   // Si temps ecoulé => deconnexion
        header('Location: deconnexion.php');
    }
    else {
        $_SESSION['timeout_idle'] = time() + 2*24*60*60; // Sinon on ralonge
    }
}


function getEtat($etat) {
    $string = '';
    switch($etat) {
        case 0:
            $string = 'Non approuvé';
            break;
        case 1:
            $string = 'Approuvé';
            break;
    }
    return $string;
}

function getCongesSalaries() {
    $pdo = new PDO('mysql:host=localhost;dbname=base_projet;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    $query = "SELECT * FROM conges c
                    INNER JOIN salarie s ON s.id_salarie = c.id_salarie";
    $stmt = $pdo->prepare($query) or die(print_r($pdo->errorInfo()));
    $stmt->execute();
    return $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

?>

<?php
    if($_SESSION['profil'] === 'admin'):
?>
<html>
    <meta charset="UTF-8">
    <title>Gestion Des Congés</title>
    <img src="Logo_esme.jpg" height="200" width="320" alt>
    <link rel="stylesheet" href="../index.css">
    <head>
        </br>
        <nav>
            <a href='gestionConges.php'>Accueil</a>
            <a href='gestionSalarie.php'>Gestion Salarié</a>
            <a href='consultationCommentaire.php'>Consultation des Commentaires</a>
            <a href='deconnexion.php'>Déconnexion</a>
            </br></br></br>
        </nav>
    </head>
    <body>
    <table style="border: solid 1px black;">
        <thead>
        <tr>
            <th>Adresse mail</th>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Fonction</th>
            <th>Date début</th>
            <th>Date fin</th>
            <th>Nombre de jours</th>
            <th>Etat</th>
            <th>Commentaire</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $conges = getCongesSalaries();
        $nbConges = count($conges);
        for($i=0;$i<$nbConges;$i++):
            $rows = $conges[$i];
            ?>
            <tr>
                <td>
                    <?= $rows['mail'] ?>
                </td>
                <td>
                    <?= $rows['nom'] ?>
                </td>
                <td>
                    <?= $rows['prenom'] ?>
                </td>
                <td>
                    <?= $rows['fonction'] ?>
                </td>
                <td>
                    <?= $rows['date_debut'] ?>
                </td>
                <td>
                    <?= $rows['date_fin'] ?>
                </td>
                <td>
                    <?= $rows['nb_jours'] ?>
                </td>
                <td>
                    <?= getEtat($rows['etat']); ?>
                </td>
                <td>
                    <?= $rows['comms'] ?>
                </td>
            </tr>
        <?php endfor; ?>
        </tbody>
    </table>
    </body>
<?php
else:
?>
    <html>
    <meta charset="UTF-8">
    <title>Gestion Des Congés</title>
    <img src="Logo_esme.jpg" height="200" width="320" alt>
    <link rel="stylesheet" href="../index.css">
    <head>
        </br>
        <nav>
            <a href='gestionConges.php'>Accueil</a>
            <a href='consultationCommentaire.php'>Consultation des Commentaires</a>
            <a href='deconnexion.php'>Déconnexion</a>
            </br></br></br>
        </nav>
    </head>
    <body>
    <p>Souhaitez-vous effectuer une demande de congé?</p><br/>
    <a href="ajoutConge.php">Faire une demande de congé</a>
    </body>
    <footer>
    Adresse : 38 rue Molière 94200 Ivry-sur-Seine <br>
    Télephone : 01 56 20 62 00
    </footer>
    </html>

<?php
endif;
?>
