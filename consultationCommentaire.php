<?php
    session_start(); // Demarrage de la session
    
    // Permet d'eviter qu'un salarié puisse aller ici
    if(!isset($_SESSION['profil']) || $_SESSION['profil'] != 'admin'){
        header('Location: index.php');
    }
    
    // Vérification de la duree de la session
    if (!isset($_SESSION['timeout_idle'])) {
        $_SESSION['timeout_idle'] = time() + 2*24*60*60; // Si existe pas on créer pour 48h
    } 
    else {
        if ($_SESSION['timeout_idle'] < time()) {   // Si temps ecoulé => deconnexion
            header('Location: deconnexion.php');
        } 
        else {
            $_SESSION['timeout_idle'] = time() + 2*24*60*60; // Sinon on ralonge
        }
    }
    include('assets/header.html');
?>


