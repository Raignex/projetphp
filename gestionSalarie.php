<?php
    session_start(); // Demarrage de la session
    
    // Permet d'eviter qu'un salarié puisse aller ici
    if(!isset($_SESSION['profil']) || $_SESSION['profil'] != 'admin'){
        header('Location: index.php');
    }
    
    // Vérification de la duree de la session
    if (!isset($_SESSION['timeout_idle'])) {
        $_SESSION['timeout_idle'] = time() + 2*24*60*60; // Si existe pas on créer pour 48h
    } 
    else {
        if ($_SESSION['timeout_idle'] < time()) {   // Si temps ecoulé => deconnexion
            header('Location: deconnexion.php');
        } 
        else {
            $_SESSION['timeout_idle'] = time() + 2*24*60*60; // Sinon on ralonge
        }
    }

    $connect = mysqli_connect("127.0.0.1","root","","base_projet");
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Gestion Des Congés</title>
        <img src="Logo_esme.jpg" height="200" width="320" alt>
    </head>
    <body>
        <nav>
            <a href="gestionConges.php">Accueil</a>
            <a href="deconnexion.php">Déconnexion</a>
	</nav>
         
        <table>
           
            <tr>
                <th>Adresse mail</th>
                <th>Nom</th>
                <th>Prénom</th>
                <th>Fonction</th>
                <th>Type de contrat</th>
                <th>Modification</th>
                <th>Supression</th>
            </tr>
            
            <?php
                $sql = "SELECT * FROM salarie";
                $result = mysqli_query($connect, $sql);           
                if (mysqli_num_rows($result) > 0) {
                // Parcourir les lignes de résultat
                    
                    while($row = mysqli_fetch_assoc($result)) { ?>
                        <form method="POST" action="suppSalarie.php"> 
                            <tr>
                                <td><?php echo $row['mail']; ?></td>
                                <td><?php echo $row['nom']; ?></td>
                                <td><?php echo $row['prenom']; ?></td>
                                <td><?php echo $row['fonction']; ?></td>
                                <td><?php echo $row['type_contrat']; ?></td>
                                <td><a href="modifSalarie.php?id=<?php echo $row['id_salarie'] ?>" >Modifier</a></td>
                                <td><input type='checkbox' name='checkbox[]' value="<?php echo $row['id_salarie']; ?>" /></td>
                            </tr>
                        
                    <?php
                    }
                    ?>
                                
                            <input type="submit" name="delete" value="delete user"/> 
                            </form>
                    <?php
                }
                
               
                if(isset($_GET["message"]))
                {
                    $message=$_GET["message"];
                    if ($message[0]=="L")
                        echo "<p style ='color:green'>".$message . "</p>";
                    else
                        echo "<p style ='color:red'>".$message . "</p>";

                }
            ?>
            <a href="ajoutSalarie.php"><input type="button" value="Ajout"></a>
        </table>
    </body>
    <footer>
        Adresse : 38 rue Molière 94200 Ivry-sur-Seine <br>
        Télephone : 01 56 20 62 00
    </footer>
</html>